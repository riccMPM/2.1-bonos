package com.pe.bonos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText eteMonto;
    Button butDonar;
    TextView tviDonacion;
    RadioGroup rgrTarjeta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eteMonto = findViewById(R.id.eteMonto);
        butDonar = findViewById(R.id.butDonar);
        tviDonacion = findViewById(R.id.tviDonacion);
        rgrTarjeta = findViewById(R.id.rgrTarjeta);


        butDonar.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {

        //Obtenemos el monto ingresado por el usuario
        int monto = Integer.parseInt(eteMonto.getText().toString());
        Double bono = 0.0;
        int resultado;

        int tarjetaSeleccionada = rgrTarjeta.getCheckedRadioButtonId();


        if (tarjetaSeleccionada == R.id.rbuVisa) {
            bono = monto * 0.1;

        } else if (tarjetaSeleccionada == R.id.rbuMastercard) {
            bono = monto * 0.4;
        }

        resultado = monto + bono.intValue();

        //Pintamos resultado

        tviDonacion.setText("Monto donado es: " + resultado);


    }
}
